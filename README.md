Torstream
=========
Voici un outil qui permet de chercher une vidéo disponible via torrent et la regarder en même temps qu'elle se télécharge.

Le lecteur vidéo pourra fonctionner car nous téléchargeons les premiers et derniers morceaux des fichiers en priorité.

Plusieurs interfaces sont disponibles : 
- torstream-cli : Utilisation simple dans un terminal.
- torstream-web : Utilisation dans un navigateur web, plus intuitif.

L'idéal serait de pouvoir l'utiliser avec
[webtorrent](https://webtorrent.io/) qui n'est malheureusement pas
compatible avec les connections TCP pour l'instant.


Installation
------------
La bibliothèque beautifulsoup4 pour python3 est nécessaire, installez-là à votre convenance selon votre système d'exploitation (py3-beautifulsoup4 pour OpenBSD).

Récupérez l'archive targz de torstream [ici](https://framagit.org/Thuban/torstream/repository/master/archive.tar.gz).
Décompressez-là.

Pour l'utiliser, vous aurez besoin des dépendances suivantes : python3-beautifulsoup4, aria2 .

Dépendances spécifiques : 
-------------------------
- torstream-cli : `aria2`
- torstream-web : `py3-flask` ([flask](http://flask.pocoo.org/)) et
  `aria2`


Utilisation de torstream-cli
-----------------------------
Lorsque vous lancez torstream, il vous demande ce que vous voulez regarder. Indiquez votre recherche, il se charge de chercher sur différents moteurs :

```
$ torstream-cli
What do you want to watch ? 
> vikings vostfr
---
### Search results with alphareign
1 - Vikings S04E19 FASTSUB VOSTFR HDTV XviD-T9
    size: 366.76 MB  seeds: 482  leeches: 6
2 - Vikings.S04E17.FASTSUB.VOSTFR.HDTV.XviD-T9
    size: 367.25 MB  seeds: 468  leeches: 7
3 - Vikings.S04E18.FASTSUB.VOSTFR.HDTV.XviD-T9
    size: 367.28 MB  seeds: 381  leeches: 13
4 - Vikings.S04E15.VOSTFR.HDTV.Xvid-EXTREME
    size: 353.03 MB  seeds: 353  leeches: 10
5 - Vikings.S04E16.FASTSUB.VOSTFR.HDTV.Xvid-EXTREME
    size: 321.49 MB  seeds: 285  leeches: 11
6 - Vikings.S04E13.FASTSUB.VOSTFR.HDTV.Xvid-EXTREME
    size: 374.57 MB  seeds: 272  leeches: 8
7 - Vikings.S04E12.FASTSUB.VOSTFR.HDTV.XViD-EXTREME
    size: 371.83 MB  seeds: 243  leeches: 4
8 - Vikings.S04E14.VOSTFR.HDTV.XViD-EXTREME.www.torrent9.biz.avi
    size: 0.00 B  seeds: 238  leeches: 4
9 - [www.Cpasbien.me] Vikings.S01E01.FASTSUB.VOSTFR.HDTV.XviD-MiND
    size: 365.18 MB  seeds: 182  leeches: 6
10 - Vikings.S01E02.VOSTFR.Gillop.avi
    size: 0.00 B  seeds: 4  leeches: 1
11 - [nextorrent.net] Vikings S04 XviD VOSTFR
    size: 7.50 GB  seeds: 38  leeches: 36
12 - Vikings.S01E01.VOSTFR.Gillop.avi
    size: 0.00 B  seeds: 2  leeches: 0
What is your choice : 
(Number, Enter for more results, ctrl-c to cancel) 
```

Une fois que vous avez le fichier que vous voulez, entrez son numéro puis validez avec Entrée.
Le téléchargement commence alors avec aria2.

```
Let's watch Vikings.S04E18.FASTSUB.VOSTFR.HDTV.XviD-T9
Press Enter to play the video when you're ready
(Enter to play)  02/14 15:23:01 [NOTICE] Downloading 1 item(s)

(Enter to play)  02/14 15:23:01 [NOTICE] DHT IPv4 : En écoute sur le port UDP 6948

(Enter to play)  02/14 15:23:01 [NOTICE] IPv4 BitTorrent: listening on TCP port 6935
```

Lorsque la progression du téléchargement vous semble suffisante (quelques %), appuyez de nouveau sur entrée pour lancer la lecture. 



Utilisation de torstream-web
-----------------------------
Lancez la commande `torstream-web`, cela va ouvrir un nouvel onglet dans
votre navigateur sur localhost:5000. Vous voilà en mesure d'utiliser
torstream dans votre navigateur.

![](torstream-accueil.png)


Configuration
-------------

Vous pouvez choisir quel lecteur vidéo sera utilisé en modifiant le fichier ~/.config/torstream. La configuration par défaut est : 

```
[DEFAULT]
videoplayer = mpv
```

Afin de préciser le dossier dans lequel seront enregistrés les torrents, ajoutez cette partie à la configuration : 

```
dowloaddir = /home/bibi/mes_downloads
```

Vous pouvez aussi modifier la liste des moteurs utilisés, ou changer l'ordre d'utilisation en éditant l'option `engines` : 

```
engines = ['leetx', 'piratebay']
```

