# -*- coding: utf-8 -*-
import os
import time
import ast
from .utils import tempdir
from .tor_engines import leetx,eztv,digbt,btsay,torrent9,idope,piratebay

# must be edited by the dev 
# FIXME
avail_engines = {'leetx': leetx.leetx,
        'eztv': eztv.eztv,
        'idope': idope.idope,
        'btsay': btsay.btsay,
        'piratebay': piratebay.piratebay,
        'torrent9': torrent9.torrent9,
        'digbt': digbt.digbt}


# cache results for later use

def cache(data, fname):
    """
    data : dict to save
    fname : file name. It is engine-keywords.cache
    """
    cache_file = os.path.join(tempdir,fname)
    with open(cache_file, 'w') as f:
        f.write(str(data))

def is_cached(fname):
    """
    if fname not too old (1 hour), return its content, else false
    """
    if not os.path.isfile(fname):
        return False

    mtime = os.path.getmtime(fname)

    if time.time() - mtime > 3600:
        return False
    else:
        with open(fname, 'r') as f:
            data = ast.literal_eval(f.read())
            return data

# search with an engine
def search(engine, sch, page=0):
    "Search with engine and return results"

    result = []
    cache_file = os.path.join(tempdir,"{}-{}.cache".format(engine, sch.replace(' ','_')))
    cached_result = is_cached(cache_file)
    if cached_result :
        result = cached_result
    else:
        if engine in avail_engines:
            result = avail_engines[engine](sch, page)
        else:
            raise Exception("Unknown engine : {}".format(engine))
        cache(result,cache_file)
    return result




# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
