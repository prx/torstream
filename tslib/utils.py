# -*- coding: utf-8 -*-

import os
import mimetypes
import configparser
import tempfile
import urllib.request
import urllib.parse

####################
# Util global variables
progname = "torstream"
progurl = "https://yeuxdelibad.net/Programmation/torstream.html"
progversion = 0.5
maxworkers = 5

configfile = os.path.expanduser('~/.config/{}'.format(progname))
defaultconfig = {'videoplayer': 'mpv', 'engines': 'idope,eztv,piratebay,leetx,torrent9,digbt,btsay'}

videoext = ['.mp4', '.avi', '.mov', '.webm', '.mpg', '.mkv', '.ogg', '.ogv' ]

tmpprefix=progname
tempdir = ''
for d in os.listdir(os.path.join('/',tempfile.template)):
    if d.startswith(tmpprefix):
        tempdir = os.path.join('/',tempfile.template,d)
        break
if tempdir == '':
    tempdir = tempfile.mkdtemp(prefix=tmpprefix)

### config
def readconfig():
    if not os.path.isfile(configfile):
        c = configparser.ConfigParser()
        c['DEFAULT'] = defaultconfig
        save_config(c)

    config = configparser.ConfigParser()
    config.read(configfile)
    return(config)

def load_config():
    c = readconfig()
    d = c.defaults()
    return d

def save_config(c):
    with open(configfile, 'w') as cf:
        c.write(cf)

c = load_config()
if 'downloaddir' in c.keys():
    if os.path.isdir(c['downloaddir']):
       tempdir = c['downloaddir']
 
dlmgtcmd = ['aria2c', '--summary-interval=3', \
        '--bt-force-encryption=true', \
        '--bt-min-crypto-level=arc4', '--bt-prioritize-piece=tail,head', \
        '--bt-require-crypto=true', '--dir={}'.format(tempdir) ]


### Functions

def htmlget(url, params=None):
    timeout = 15
    opener = urllib.request.build_opener()
    opener.addheaders = [('User-agent', 'Mozilla/5.0')]
    if params != None: 
        p = urllib.parse.urlencode(params)
        if url.endswith('?') or url.endswith('&'):
            url = '{}{}'.format(url,p)
            response = opener.open(url, timeout=timeout)
        else:
            p = p.encode('ascii')
            response = opener.open(url, p, timeout=timeout)
    else:
        response = opener.open(url, timeout=timeout)

    if response.getcode() != 200:
        return False

    data = response.read()      
    text = data.decode('utf-8')
    response.close()
    return(text)



def convb(nbbyte):
    """
    Convert a number of bytes to a good unit

    Args:
        nbbyte (int): Number to convert

    Return:
        Return a string
    """
    l = len(str(nbbyte))
    if l < 4:
        return "{0:.2f} B".format(nbbyte)
    elif l < 7:
        return "{0:.2f} kB".format(nbbyte / 1000)
    elif l < 10:
        return "{0:.2f} MB".format(nbbyte / 1000000)
    else:
        return "{0:.2f} GB".format(nbbyte / 1000000000)


def find_video(path):
    if not os.path.exists(path):
        return ''
    fl = os.listdir(path)
    for f in fl:
        fp = os.path.join(path,f)
        if os.path.isdir(fp):
            return(find_video(fp))
        else:
            t = mimetypes.guess_type(fp)[0]
            if t:
                if 'video' in t:
                    return(fp)
                    break
            else:
                for e in videoext:
                    if fp.lower().endswith(e):
                        return(fp)
                        break

    return('')


