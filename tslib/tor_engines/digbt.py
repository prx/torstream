import bs4
import concurrent.futures

from ..utils import *

baseurl = 'https://www.digbt.org'

def digbt(sch, page=0):
    """
    Return:
        List of dictionnaries with keys :
            name : torrent name
            magnet : magnet link
            size : size
            seed : number of seeds
    """
    reslist = []
    page = int(page) + 1
    url = '{}/search/{}-time-{}'.format(baseurl, sch, page)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")

    name = str()
    magnet = str()
    size = str()
    seeds = '?'
    leechs = '?'
    res = {}

    for r in soup.find_all('td', class_='x-item'):
        for a in r.find_all('a', class_='title'):
            if a['href'].startswith('magnet:?'):
                magnet = a['href']
            else:
                name = a.string

        d = r.find('div', class_='tail').text
        d = d.strip().split(' ') # ugly, FIXME
        size = d[3]

        if name and magnet and size :
            res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}
            name = str()
            magnet = str()
            size = str()

            reslist.append(res)

    return(reslist)
