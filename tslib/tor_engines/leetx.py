import bs4
import concurrent.futures

from ..utils import *

baseurl = 'https://1337x.to'


def leetx(sch, page=0):
    page = int(page) + 1
    sch = sch.replace(' ', '+')
    url = '{}/search/{}/page/'.format(baseurl,sch, page)
    s = htmlget(url)
    soup = bs4.BeautifulSoup(s, "html.parser")
    reslist = []

    with concurrent.futures.ThreadPoolExecutor(max_workers=maxworkers) as executor:
        futures = [ executor.submit(leetx_parse, tr) for tr in soup.find_all("tr") ]
        reslist = [future.result() for future in concurrent.futures.as_completed(futures) if future.result() != None ]

    return reslist

def leetx_parse(tr):
    name = str()
    magnet = str()
    size = str()
    seeds = str()
    leechs = str()
    res = {}

    name = tr.find('td', class_='name')
    if name :
        for a in name.find_all("a"):
            if a['href'].startswith("/torrent"):
                mgtlink = a['href']
                mgturl = "{}{}".format(baseurl,mgtlink)
                magnet = leetx_get_magnetlink(mgturl)
        name = name.text
    seeds = tr.find('td', class_='seeds')
    if seeds : 
        seeds = seeds.text
    leechs = tr.find('td', class_='leeches')
    if leechs:
        leechs = leechs.text
    size = tr.find('td', class_='size')
    if size:
        size = size.text.split('B')[0]

    if name and magnet and size and seeds and leechs :
        res = {'name': name, 'magnet': magnet, 'size': size, 'seeds': seeds, "leechs": leechs}
        return res

def leetx_get_magnetlink(mgturl):
    mhtml = htmlget(mgturl)
    mgtsoup = bs4.BeautifulSoup(mhtml, "html.parser")
    magnet = ""
    for a in mgtsoup.find_all("a"):
        if a['href'].startswith("magnet:?"):
            magnet = a['href']
            break
    return magnet

